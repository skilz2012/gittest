package com.test.tech;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.tech.server.ServerApi;
import com.test.tech.server.entities.ContributorObject;
import com.test.tech.server.entities.UserObject;

import java.util.ArrayList;
import java.util.List;

class ContributorsListAdapter extends RecyclerView.Adapter<ContributorsListAdapter.ViewHolder> {
    private List<ContributorObject> mData;
    private Context context;

    public ContributorsListAdapter(Context context) {
        this.context = context;
        mData = new ArrayList<>();
    }

    public void setData(List<ContributorObject> contributorObjects) {
        if (contributorObjects == null) {
            contributorObjects = new ArrayList<>();
        }
        mData = contributorObjects;
        notifyDataSetChanged();
    }

    @Override
    public ContributorsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contributor_item, parent, false);
        return new ContributorsListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ContributorsListAdapter.ViewHolder holder, final int position) {
        final ContributorObject contributorObject = mData.get(position);
        holder.loginContibutors.setText(contributorObject.login + " -  Contributions: " + contributorObject.contributions);
        Glide.with(context).load(contributorObject.avatarUrl).into(holder.userImage);
        holder.detailsContainer.setVisibility(View.GONE);
        holder.showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServerApi.General.getUserInfo(context, contributorObject.login, new ServerApi.OnFinishedListener<UserObject>() {
                    @Override
                    public void onFinishedListener(final UserObject result) {
                        if (result != null) {
                            holder.detailsContainer.setVisibility(View.VISIBLE);
                            if(result.company==null || result.company.equals("null") || result.company.length() == 0){
                                holder.companyText.setText("Company: No Info");
                            } else {
                                holder.companyText.setText("Company: " + result.company);
                            }

                            if(result.location==null || result.location.equals("null") || result.location.length() == 0) {
                                holder.locationText.setVisibility(View.GONE);
                            } else {
                                holder.locationText.setVisibility(View.VISIBLE);
                                final SpannableString content = new SpannableString("Location: " + result.location);
                                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                                holder.locationText.setText(content);

                                holder.locationText.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent i = new Intent(context, MapActivity.class);
                                        i.putExtra("location", result.location);
                                        context.startActivity(i);
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<ContributorObject> getData() {
        return mData;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView companyText;
        TextView loginContibutors;
        TextView locationText;
        ImageView userImage;
        ImageView showMore;
        View detailsContainer;

        public ViewHolder(View v) {
            super(v);
            userImage = v.findViewById(R.id.user_image);
            loginContibutors = v.findViewById(R.id.login_contibutors);
            showMore = v.findViewById(R.id.show_more);
            detailsContainer = v.findViewById(R.id.details_container);
            companyText = v.findViewById(R.id.company_text);
            locationText = v.findViewById(R.id.location_text);
        }
    }
}
