package com.test.tech.tools;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.test.tech.views.ProgressHUD;

import java.util.List;


public class AsyncTools {

    /**
     * Method for AsyncTask.
     *
     * @param asyncCallBack callback.
     */
    public static void async(final AsyncCallBack asyncCallBack) {
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                return asyncCallBack.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                asyncCallBack.onPostExecute(s);
            }
        }.execute();
    }


    private static <T> List<T> pushBack(List<T> list, Class<T> typeKey) throws Exception {
        list.add(typeKey.getConstructor().newInstance());
        return list;
    }

    /**
     * Async with progress bar.
     *
     * @param context              main context.
     * @param showProgress
     * @param onPreExecuteCallBack callback.
     */
    public static void apiAsyncProgress(final Context context, final boolean showProgress, final AsyncCallBack onPreExecuteCallBack) {
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            protected ProgressHUD progressHUD;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress) {
                    progressHUD = ProgressHUD.show(context,"", false, null);
                }
            }

            @Override
            protected String doInBackground(String... params) {
                return onPreExecuteCallBack.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (showProgress && progressHUD != null) {
                    progressHUD.hideDialog();
                }
                if (onPreExecuteCallBack != null) {
                    onPreExecuteCallBack.onPostExecute(s);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static AsyncTask<String, Void, String> apiAsyncProgressWith(final Context context, final boolean showProgress, final AsyncCallBack onPreExecuteCallBack) {
        return new AsyncTask<String, Void, String>() {
            protected ProgressHUD progressHUD;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress) {
                    progressHUD = ProgressHUD.show(context, "", false, null);
                }
            }

            @Override
            protected String doInBackground(String... params) {
                return onPreExecuteCallBack.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (showProgress) {
                    progressHUD.hideDialog();
                }
                if (onPreExecuteCallBack != null) {
                    onPreExecuteCallBack.onPostExecute(s);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public static void apiCustomAsyncProgress(final Context context, final boolean showProgress, final AsyncCallBack onPreExecuteCallBack) {
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            protected ProgressHUD progressHUD;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress) {
                    progressHUD = ProgressHUD.show(context, "", false, null);
                }
            }

            @Override
            protected String doInBackground(String... params) {
                return onPreExecuteCallBack.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (showProgress) {
                    progressHUD.hideDialog();
                }
                if (onPreExecuteCallBack != null) {
                    onPreExecuteCallBack.onPostExecute(s);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static <T> void asyncList(final Context context, final boolean showProgress, final AsyncCustom<T> asyncCustom) {
        AsyncTask<String, Void, T> asyncTask = new AsyncTask<String, Void, T>() {
            protected ProgressHUD progressHUD;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress) {
                    progressHUD = ProgressHUD.show(context, "", false, null);
                }
            }

            @Override
            protected T doInBackground(String... params) {
                return asyncCustom.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(T s) {
                super.onPostExecute(s);
                if (showProgress) {
                    progressHUD.hideDialog();
                }
                if (asyncCustom != null) {
                    asyncCustom.onPostExecute(s);
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public static void apiSearch(final Context context, final ImageView spinnerImage, final RelativeLayout spinnerContainer, final AsyncCallBack onPreExecuteCallBack) {
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                spinnerContainer.setVisibility(View.VISIBLE);
                spinnerImage.setVisibility(View.VISIBLE);
                AnimationDrawable spinner = (AnimationDrawable) spinnerImage.getBackground();
                spinner.start();

            }

            @Override
            protected String doInBackground(String... params) {
                return onPreExecuteCallBack.doInBackgroundCallback();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                spinnerImage.setVisibility(View.GONE);
                spinnerContainer.setVisibility(View.GONE);
                if (onPreExecuteCallBack != null) {
                    onPreExecuteCallBack.onPostExecute(s);
                }
            }
        }.execute();
    }


    public interface AsyncCustom<T> {

        T doInBackgroundCallback();

        void onPostExecute(T result);
    }

    public interface AsyncCallBack {

        String doInBackgroundCallback();

        void onPostExecute(String result);
    }

    public interface StringCallBack {

        void asyncCallback(String s);
    }
}
