package com.test.tech;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.test.tech.server.ServerApi;
import com.test.tech.server.entities.ContributorObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView contributorsList;
    private ContributorsListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contributorsList = findViewById(R.id.recycler_view);
        contributorsList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        listAdapter = new ContributorsListAdapter(MainActivity.this);
        contributorsList.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServerApi.General.getContributors(MainActivity.this, new ServerApi.OnFinishedListener<List<ContributorObject>>() {
            @Override
            public void onFinishedListener(List<ContributorObject> result) {
                listAdapter.setData(result);
            }
        });
    }
}
