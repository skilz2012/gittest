package com.test.tech.server;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.test.tech.GitTestApplication;
import com.test.tech.server.entities.ContributorObject;
import com.test.tech.server.entities.UserObject;
import com.test.tech.tools.AsyncTools;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;


public class ServerApi {

    private static final String CONTENT_TYPE = "content-type";
    private static final String CONTENT_TYPE_VALUE = "application/json";

    public static class General {

        public static void getContributors(final Context context, final OnFinishedListener<List<ContributorObject>> onFinishedListener) {
            final String url = HttpUrl.parse(ServerConstants.GET_DEFAULT_CONTRIBUTORS).newBuilder()
                    .build().toString();

            final Headers headers = new Headers.Builder()
                    .add(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                    .build();

            AsyncTools.apiAsyncProgress(context, false, new AsyncTools.AsyncCallBack() {
                @Override
                public String doInBackgroundCallback() {
                    ServerApi serverApi = new ServerApi();
                    return serverApi.sendGetRequest(context, headers, url);
                }

                @Override
                public void onPostExecute(String result) {
                    List<ContributorObject> contributorObjects = new ArrayList<>();
                    Gson gson = new Gson();
                    Type typeRes = new TypeToken<List<ContributorObject>>() {
                    }.getType();
                    contributorObjects = gson.fromJson(result, typeRes);
                    onFinishedListener.onFinishedListener(contributorObjects);
                }
            });
        }

        /**
         *
         * @param userLogin "login" field from getContributors
         */
        public static void getUserInfo(final Context context,String userLogin, final OnFinishedListener<UserObject> onFinishedListener) {
            final String url = HttpUrl.parse(ServerConstants.GET_USER_INFO + "/" + userLogin).newBuilder()
                    .build().toString();

            final Headers headers = new Headers.Builder()
                    .add(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                    .build();

            AsyncTools.apiAsyncProgress(context, false, new AsyncTools.AsyncCallBack() {
                @Override
                public String doInBackgroundCallback() {
                    ServerApi serverApi = new ServerApi();
                    return serverApi.sendGetRequest(context, headers, url);
                }

                @Override
                public void onPostExecute(String result) {
                    UserObject userObject;
                    Gson gson = new Gson();
                    Type typeRes = new TypeToken<UserObject>() {
                    }.getType();
                    userObject = gson.fromJson(result, typeRes);
                    onFinishedListener.onFinishedListener(userObject);
                }
            });
        }

    }

    /**
     * @param request RequestBody for reading
     * @return string version of RequestBody
     */
    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "can't read body";
        }
    }

    /**
     * Sending GET request.
     *
     * @param context context.
     * @param headers necessary item for header part.
     * @param url     with params.
     * @return result.
     */
    public String sendGetRequest(Context context, Headers headers, String url) {
        return sendGetRequest(context, headers, url, null);
    }

    /**
     * Sending GET request.
     *
     * @param context context.
     * @param headers necessary item for header part.
     * @param url     with params.
     * @param tag     tag for cancel.
     * @return result.
     */
    public String sendGetRequest(Context context, Headers headers, String url, String tag) {
            headers = headers.newBuilder()
                    .add("Accept", CONTENT_TYPE_VALUE)
                    .build();

            OkHttpClient client = ((GitTestApplication) context.getApplicationContext()).getHttpClient();

            Request.Builder build = new Request.Builder()
                    .headers(headers)
                    .url(url);
            if (tag != null) {
                build.tag(tag);
            }

            Request request = build.build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (response != null) {
                    String result = response.body().string();
                    if (response != null) {
                        response.close();
                    }
                    return result;
                } else {
                    if (response != null) {
                        response.close();
                    }
                    return "error";
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (response != null) {
                    response.close();
                }
                return "error";
            }

    }


    /**
     *
     * @param <T> necessary type
     */
    public interface OnFinishedListener<T> {
        void onFinishedListener(T result);
    }

}
