package com.test.tech.server;

public class ServerConstants {
    private static final String BASE_URL = "https://api.github.com";
    public static final String GET_DEFAULT_CONTRIBUTORS = BASE_URL+"/repos/bumptech/glide/contributors";
    public static final String GET_USER_INFO = BASE_URL+"/users";


}
